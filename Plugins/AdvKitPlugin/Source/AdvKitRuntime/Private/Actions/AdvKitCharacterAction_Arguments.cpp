#include "AdvKitRuntime.h"
#include "Actions/AdvKitCharacterAction_Arguments.h"

UAdvKitCharacterAction_Arguments* UAdvKitCharacterAction_Arguments::CreateArguments(UObject* WorldContextObject, TSubclassOf<UAdvKitCharacterAction_Arguments> ArgumentClass)
{
	if (!IsValid(ArgumentClass))
	{
		return nullptr;
	}

	return NewObject<UAdvKitCharacterAction_Arguments>(WorldContextObject, ArgumentClass);
}

bool UAdvKitCharacterAction_Arguments::IsSupportedForNetworking() const
{
	return true;
}

bool UAdvKitCharacterAction_Arguments::IsNameStableForNetworking() const
{
	return false;
}
