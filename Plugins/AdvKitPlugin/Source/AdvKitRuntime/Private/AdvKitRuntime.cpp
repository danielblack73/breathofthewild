#include "AdvKitRuntime.h"

/*! \mainpage 
*
* \section intro_sec Introduction
*
* Welcome to the Adventure Kit API documentation. You can find the wiki documentation <a href="https://github.com/FreetimeStudio/Adventure-Kit-Public/wiki">here</a>.
*
* \section install_sec Installation
* Check the setup guide for everything you need to do to get the kit running: <a href="https://github.com/FreetimeStudio/Adventure-Kit-Public/wiki/Setup">Setup Guide</a>
*
* \section quicklinks_sec Quick Links
* Links to quickly get to the section you want.
*
* \subsection Characters
* - @ref AAdvKitCharacter
*	- @ref UAdvKitCharacterMovementComponent
* - @ref AAdvKitGravityCharacter
*	- @ref UAdvKitGravityCharacterMovementComponent
*   - @ref AAdvKitGravityPlayerController
*
* \subsection Inventory
* - @ref AAdvKitInventoryManager
* - @ref AAdvKitInventoryItem
* - @ref AAdvKitWeapon
* - @ref AAdvKitInventoryPickup
* - @ref AAdvKitUsable
*
* \subsection Actions
* - @ref AAdvKitCharacterActionManager
* - @ref UAdvKitCharacterAction
*
*
* \subsection Zones
* - @ref AAdvKitZone
*   - @ref AAdvKitZoneLine
*   - @ref AAdvKitZoneRectangle
* - @ref UAdvKitTransitionComponent
*   - @ref UAdvKitTransitionComponentPoint
*   - @ref UAdvKitTransitionComponentArea
*/

DEFINE_LOG_CATEGORY(LogAdvKit);

/** @brief Module to register runtime classes of the adventure kit. */
class FAdvKitRuntime : public IModuleInterface
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

IMPLEMENT_MODULE(FAdvKitRuntime, AdvKitRuntime)


void FAdvKitRuntime::StartupModule()
{

}


void FAdvKitRuntime::ShutdownModule()
{

}



